import pandas
import json
import logging
import datetime
import pymongo
import hashlib
import os
from flask import g
import pandas as pd
from apis.db_pg_api import DBPostgres
from apis.db_neo4j_api import main_neo4j_conn
from sqlalchemy.sql import text
from geopy.geocoders import Nominatim
from configs.env_config import ENV_CONFIG
from collections import defaultdict
from apis.db_mongo_api import main_mongo_db
from models.tracing_management.request.insert_identification_model import InsertIdentificationModel
from models.tracing_management.request.update_identification_model import UpdateIdentificationModel
from models.tracing_management.request.insert_target_model import InsertTargetModel
from models.tracing_management.request.update_target_model import UpdateTargetModel
from bson.objectid import ObjectId
from exceptions.upload_exception import UploadException
import csv


class IdentificationQuery:
    identification_table = main_mongo_db['identifications']
    target_table = main_mongo_db['targets']
    configuration_table = main_mongo_db['configurations']

    def insert_identification(self, model: InsertIdentificationModel):
        data = model.__dict__
        id = self.identification_table.insert_one(data).inserted_id
        return id

    def update_identification(self, model: UpdateIdentificationModel):
        self.identification_table.update({'_id': model.id}, {
            '$set': {
                'name': model.name,
                'date': model.date,
                'description': model.description,
                'targets': model.targets,
                'updated_by': model.updated_by,
                'updated_date': model.updated_date
            }}, multi=True)

    def delete_identification(self, model: UpdateIdentificationModel):
        self.identification_table.update({'_id': model.id}, {
            '$set': {
                'deleted_by': model.deleted_by,
                'deleted_date': model.deleted_date
            }}, multi=True)

    def data_identification_targets(self, key):
        data = list(self.target_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'phone': {"$regex": key, "$options": "i"}
            }]
        }).sort([('created_date', -1)]).limit(10))
        targets = []
        for d in data:
            target = {
                'value': d['name'] + '-' + d['phone'],
                'text': d['name'] + ' - ' + d['phone']
            }
            targets.append(target)
        return targets

    def data_identification_by_name(self, name):
        data = list(self.identification_table.find({
            'deleted_by': None,
            'name': name
        }))
        return data

    def data_identification_by_id(self, key):
        if key == '':
            return {'targets': []}
        data = self.identification_table.find_one({
            'deleted_by': None,
            '_id': ObjectId(key)
        })
        return data

    def data_identification_by_name_regex(self, key, limit):
        data = list(self.identification_table.find({
            'deleted_by': None,
            'name': {"$regex": key, "$options": "i"}
        }, {'name': 1}).sort([('created_date', -1)]).limit(limit))
        return data

    def data_identification_by_target(self, target):
        data = list(self.identification_table.aggregate([
            {
                '$match': {
                    'targets': {
                        '$in': [{
                            'name': target['name'],
                            'phone': target['phone']
                        }]
                    }
                }
            }
        ]))
        return data

    def insert_target(self, model: InsertTargetModel):
        data = model.__dict__
        id = self.target_table.insert_one(data).inserted_id
        return id

    def update_target(self, model: UpdateTargetModel):
        self.target_table.update({'_id': model.id}, {
            '$set': {
                'phone': model.phone,
                'name': model.name,
                'status': model.status,
                'identity': model.identity,
                'updated_by': model.updated_by,
                'updated_date': model.updated_date
            }}, multi=True)

    def delete_target(self, model: UpdateIdentificationModel):
        self.target_table.update({'_id': model.id}, {
            '$set': {
                'deleted_by': model.deleted_by,
                'deleted_date': model.deleted_date
            }}, multi=True)

    def data_target(self, limit, offset, key, sort):
        data = list(self.target_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'phone': {"$regex": key, "$options": "i"}
            }]
        }).sort([('created_date', -1)]).skip(offset).limit(limit))
        return data

    def data_target_count(self, key):
        data = list(self.target_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'phone': {"$regex": key, "$options": "i"}
            }]
        }))
        result = len(data)
        return result

    def data_target_by_id(self, key):
        data = self.target_table.find_one({
            'deleted_by': None,
            '_id': key
        })
        return data

    def delete_ipdr_loc(self, phone, name):
        pg_db = DBPostgres()
        sql_query = f"DELETE FROM ipdr_loc WHERE phone='{phone}' AND target_name='{name}'"
        pg_db.execute(sql_query)

    def delete_ipdr_loc_tracing(self, phone, name):
        pg_db = DBPostgres()
        sql_query = f"DELETE FROM ipdr_loc_tracing WHERE phone='{phone}' AND target_name='{name}'"
        pg_db.execute(sql_query)

    def update_ipdr_loc(self, phone_old, phone_new, name_old, name_new):
        pg_db = DBPostgres()
        sql_query = f"UPDATE ipdr_loc SET phone='{phone_new}', target_name='{name_new}' WHERE phone='{phone_old}' AND target_name='{name_old}'"
        pg_db.execute(sql_query)

    def update_ipdr_loc_tracing(self, phone_old, phone_new, name_old, name_new):
        pg_db = DBPostgres()
        sql_query = f"UPDATE ipdr_loc_tracing SET phone='{phone_new}', target_name='{name_new}' WHERE phone='{phone_old}' AND target_name='{name_old}'"
        pg_db.execute(sql_query)

    def data_registration(self, phone):
        data = self.configuration_table.aggregate([
            {
                '$match': {
                    'name': 'phone_registration',
                    'type': 'web_service'
                }
            }
        ])
        reg_result = None
        result = None
        if reg_result is None:
            result = {
                'status': 'Data not found',
                'identity': None
            }
        return result

    def data_target_by_phone(self, phone):
        data = self.target_table.find_one({
            'deleted_by': None,
            'phone': phone
        })
        return data

    def data_target_by_id_phone(self, id, phone):
        data = self.target_table.find_one({
            '_id': id,
            'deleted_by': None,
            'phone': phone
        })
        return data

    def data_target_by_phones(self, phones):
        data = list(self.target_table.find({
            'deleted_by': None,
            'phone': {'$in': phones}
        }))
        return data

    def upload_target(self, file):
        try:
            # file_config = ENV_CONFIG['file']
            # filename = file.filename.replace('.txt', '.csv')
            # print('filename : ', filename)
            # file_content = file.read()
            # file_hash = hashlib.md5(file_content).hexdigest()
            # file_path = os.path.join(
            #     file_config['media_upload_dir'], f'{file_hash}_{filename}')
            # print('file_path : ', file_path)
            # with open(file_path, 'wb') as f:
            #     f.write(file_content)

            target_list = []
            with open(file) as csvfile:
                reader = csv.reader(csvfile, delimiter=',', quotechar='|')
                count = 0
                for row in reader:
                    print('row = ', row)
                    if count > 0:
                        target = {
                            'name': row[1],
                            'phone': row[0]
                        }
                        target_list.append(target)
                    count = count + 1
            return target_list
        except UploadException as exc:
            logging.error(str(exc))
            result = {
                'code': 'UPLOAD_ERROR',
                'message': str(exc)
            }
            print(result)
            return []
