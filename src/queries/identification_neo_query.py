import pandas
import json
import logging
import datetime
import pandas as pd
from apis.db_neo4j_api import main_neo4j_conn
from configs.env_config import ENV_CONFIG
from collections import defaultdict


class IdentificationNeoQuery:

    def insert_identification(self, id, data):
        user_r = []
        user_w = []
        group_r = []
        group_w = []
        for ur in data['_user_read']:
            user_r.append(str(ur))
        for uw in data['_user_write']:
            user_w.append(str(uw))
        for gr in data['_group_read']:
            group_r.append(str(gr))
        for gw in data['_group_write']:
            group_w.append(str(gw))
        data = {
            'id': str(id),
            'name': data['name'],
            'period': data['period_start'] + ' to ' + data['period_end'],
            'date': data['date'],
            'description': data['description'],
            'user_read': str(user_r),
            'user_write': str(user_w),
            'group_read': str(group_r),
            'group_write': str(group_w)
        }
        main_neo4j_conn.write_transaction(
            self.insert_identification_query, data)

    def insert_identification_query(self, tx, data):
        query = 'MERGE (a:IDENTIFICATION {identification_id: $id}) ON CREATE SET a.name = $name, a.period = $period, a.date = $date, a.description = $description, a.user_read=$user_read, a.user_write=$user_read, a.group_read=$group_read, a.group_write=$group_write return ID(a) as node_id'
        tx.run(query, **data)

    def update_identification(self, id, data):
        user_r = []
        user_w = []
        group_r = []
        group_w = []
        for ur in data['_user_read']:
            user_r.append(str(ur))
        for uw in data['_user_write']:
            user_w.append(str(uw))
        for gr in data['_group_read']:
            group_r.append(str(gr))
        for gw in data['_group_write']:
            group_w.append(str(gw))
        data = {
            'id': str(id),
            'name': data['name'],
            'period': data['period_start'] + ' to ' + data['period_end'],
            'date': data['date'],
            'description': str(data['description']),
            'user_read': str(user_r),
            'user_write': str(user_w),
            'group_read': str(group_r),
            'group_write': str(group_w)
        }
        main_neo4j_conn.write_transaction(
            self.update_identification_query, data)

    def update_identification_query(self, tx, data):
        query = 'MATCH (a:IDENTIFICATION {identification_id: $id}) SET a.name = $name, a.period = $period, a.date = $date, a.description = $description, a.user_read=$user_read, a.user_write=$user_read, a.group_read=$group_read, a.group_write=$group_write'
        tx.run(query, **data)

    def delete_identification(self, id):
        data = {
            'id': id
        }
        main_neo4j_conn.write_transaction(
            self.delete_identification_query, data)

    def delete_identification_query(self, tx, data):
        query = 'MATCH (a:IDENTIFICATION) WHERE a.identification_id=$id DETACH DELETE a'
        tx.run(query, **data)

    def delete_identification_relation(self, id):
        data = {
            'id': id
        }
        main_neo4j_conn.write_transaction(
            self.delete_identification_relation_query, data)

    def delete_identification_relation_query(self, tx, data):
        query = 'MATCH (a:IDENTIFICATION {idenftication_id: $id})-[r]->() DELETE r'
        tx.run(query, **data)

    def insert_target(self, id, data):
        data = {
            'id': str(id),
            'name': data['name'],
            'phone': data['phone']
        }
        main_neo4j_conn.write_transaction(
            self.insert_target_query, data)

    def insert_target_query(self, tx, data):
        query = 'MERGE (a:TARGET {target_id: $id}) ON CREATE SET a.name = $name, a.phone = $phone return ID(a) as node_id'
        tx.run(query, **data)

    def update_target(self, id, data):
        data = {
            'id': str(id),
            'name': data['name'],
            'phone': data['phone']
        }
        main_neo4j_conn.write_transaction(
            self.update_target_query, data)

    def update_target_query(self, tx, data):
        query = 'MATCH (a:TARGET {target_id: $id}) SET a.name = $name, a.phone = $phone'
        tx.run(query, **data)

    def delete_target(self, id):
        data = {
            'id': id
        }
        main_neo4j_conn.write_transaction(
            self.delete_target_query, data)

    def delete_target_query(self, tx, data):
        query = 'MATCH (a:TARGET) WHERE a.target_id=$id DETACH DELETE a'
        tx.run(query, **data)

    def create_link_identification_to_target(self, identification_id, targets):
        for t in targets:
            data = {
                'identification_id': identification_id,
                'target_phone': t['phone']
            }
            main_neo4j_conn.write_transaction(
                self.create_link_indentification_to_target_query, data)

    def create_link_indentification_to_target_query(self, tx, data):
        query = 'MATCH (m: IDENTIFICATION), (n: TARGET) WHERE m.identification_id="' + data['identification_id'] + \
            '" AND n.phone="' + data['target_phone'] + \
                '" MERGE (n)-[r:OWN_TARGET]->(m) RETURN m'
        tx.run(query, id=data['identification_id'])

    def insert_movement(self, id, phone, aggregate):
        data = {
            'id': str(id),
            'phone': phone,
            'aggregate': aggregate
        }
        main_neo4j_conn.write_transaction(
            self.insert_movement_query, data)

    def insert_movement_query(self, tx, data):
        query = 'MERGE (a:MOVEMENT {target_id: $id}) ON CREATE SET a.phone = $phone, a.aggregate = $aggregate return ID(a) as node_id'
        tx.run(query, **data)

    def update_movement(self, id, phone, aggregate):
        data = {
            'id': str(id),
            'phone': phone,
            'aggregate': aggregate
        }
        main_neo4j_conn.write_transaction(
            self.insert_movement_query, data)

    def update_movement_query(self, tx, data):
        query = 'MATCH (a:MOVEMENT {target_id: $id}) SET a.phone = $phone, a.aggregate = $aggregate return ID(a) as node_id'
        tx.run(query, **data)

    def create_link_target_to_movement(self, phone):
        data = {
            'phone': phone
        }
        main_neo4j_conn.write_transaction(
            self.create_link_target_to_movement_query, data)

    def create_link_target_to_movement_query(self, tx, data):
        query = 'MATCH (m: TARGET), (n: MOVEMENT) WHERE m.phone="' + data['phone'] + \
            '" AND n.phone="' + data['phone'] + \
                '" MERGE (n)-[r:OWN_MOVEMENT]->(m) RETURN m'
        tx.run(query, id=data['phone'])

    def create_link_target_to_person(self, phone, nik):
        data = {
            'phone': phone,
            'nik': str(nik)
        }
        main_neo4j_conn.write_transaction(
            self.create_link_target_to_person_query, data)

    def create_link_target_to_person_query(self, tx, data):
        query = 'MATCH (m: TARGET), (n: PERSON) WHERE m.phone="' + data['phone'] + \
            '" AND n.nik="' + data['nik'] + \
                '" MERGE (n)-[r:OWN_IDENTITY]->(m) RETURN m'
        tx.run(query, id=data['phone'])

    def delete_target_relation(self, phone):
        data = {
            'phone': phone
        }
        main_neo4j_conn.write_transaction(
            self.delete_target_relation_query, data)

    def delete_target_relation_query(self, tx, data):
        query = 'MATCH (a:TARGET)-[r]->() WHERE a.phone="' + \
            data['phone'] + '" DELETE r'
        tx.run(query, **data)
