import pandas
import json
import logging
import datetime
import pymongo
import hashlib
import os
import pandas as pd
import csv
from apis.db_pg_api import DBPostgres
from sqlalchemy.sql import text
from geopy.geocoders import Nominatim
from configs.env_config import ENV_CONFIG
from exceptions.upload_exception import UploadException
from collections import defaultdict
from apis.db_pg_api import DBPostgres
from apis.db_mongo_api import main_mongo_db
from models.tracing_management.request.insert_identification_model import InsertIdentificationModel
from models.tracing_management.request.update_identification_model import UpdateIdentificationModel
from models.tracing_management.request.insert_target_model import InsertTargetModel
from models.tracing_management.request.update_target_model import UpdateTargetModel
from bson.objectid import ObjectId
from turfpy.measurement import boolean_point_in_polygon
from geojson import Point, Polygon, Feature


class TracingQuery:
    identification_table = main_mongo_db['identifications']
    tracing_target_table = main_mongo_db['tracing_target']
    notification_table = main_mongo_db['notification']
    alert_management_table = main_mongo_db['alert_management']

    def upload(self, phone, target, file, user_id: str):
        try:
            # file_config = ENV_CONFIG['file']
            # filename = file.filename.replace('.txt', '.csv')
            # file_content = file.read()
            # file_hash = hashlib.md5(file_content).hexdigest()
            file_path = file

            # with open(file_path, 'wb') as f:
            #     f.write(file_content)

            columns = defaultdict(list)
            tmp_coordinates = None
            tmp_village = None
            tmp_city = None
            tmp_province = None
            upload_time = datetime.datetime.now()

            # latest location
            # latest_location = self.target_location(target, phone)

            with open(file_path) as csvfile:
                # read rows into a dictionary format
                reader = csv.DictReader(csvfile)
                values = []
                # read a row as {column1: value1, column2: value2,...}
                for row in reader:
                    for (k, v) in row.items():  # go over each column name and value
                        # append the value into the appropriate list
                        columns[k] = v
                        # based on column name k
                    is_exist = self.ipdr_loc_exist(phone, columns['Timestamp'])
                    if is_exist == False:
                        columns['Geolocation'] = str(
                            columns['Geolocation']).replace('"', '')
                        if columns['Geolocation'] == '':
                            columns['Village'] = ''
                            columns['City'] = ''
                            columns['Province'] = ''
                        else:
                            if columns['Geolocation'] != tmp_coordinates:
                                tmp_coordinates = columns['Geolocation']
                                location = self.geolocation(
                                    columns['Geolocation'])
                                if location['city'] == 'Daerah Khusus Ibukota Jakarta':
                                    columns['Village'] = location['village']
                                    columns['City'] = location['city']
                                    columns['Province'] = location['city']
                                    tmp_village = location['village']
                                    tmp_city = location['city']
                                    tmp_province = location['city']
                                else:
                                    columns['Village'] = location['village']
                                    columns['City'] = location['city']
                                    columns['Province'] = location['province']
                                    tmp_village = location['village']
                                    tmp_city = location['city']
                                    tmp_province = location['province']
                            else:
                                columns['Village'] = tmp_village
                                columns['City'] = tmp_city
                                columns['Province'] = tmp_province
                        values.append(f"('{columns['Timestamp']}', '{columns['Source']}', '{columns['CGI']}', '{columns['Village']}', '{columns['City']}', '{columns['Province']}', '{columns['Country']}', '{columns['Geolocation']}', '{columns['RAT']}',  '{columns['SID']}', '{columns['LAC']}', '{columns['CI/ECI']}', '{columns['Outbound Roaming']}', '{columns['MCC']}', '{columns['MNC']}', '{columns['Operator']}', '{columns['Estimated Location']}', '{target}', '{phone}', '{upload_time}')")

                # insert to database
                print(len(values))
                if len(values) > 0:
                    final_values = (',').join(values)
                    self.insert_into_db(final_values)

                    # update last location
                    # self.update_last_location(target)

                    # latest location
                    # last_location = self.target_location(target, phone)

                    self.insert_target_tracing(target, phone)

                result_moving = self.get_notification(phone, target, user_id)
                count_result = len(result_moving)
                if count_result > 0:
                    data = {
                        'target': target,
                        'phone': phone,
                        'geolocation': result_moving,
                        'description': target + ' Berpindah ke ' + ', '.join(result_moving),
                        'status': 'unread',
                        'created_by': user_id,
                        'created_date': datetime.datetime.now(),
                        'updated_by': None,
                        'updated_date': None,
                        'deleted_by': None,
                        'deleted_date': None
                    }
                    self.notification_table.insert_one(data).inserted_id
                self.get_notification_alert_management(phone, target, user_id)

            result = {
                'code': 'UPLOAD_SUCCESS',
                'message': 'Upload success'
            }
            return result
        except UploadException as exc:
            logging.error(str(exc))
            result = {
                'code': 'UPLOAD_ERROR',
                'message': str(exc)
            }
            return result

    def ipdr_loc_exist(self, phone, timestamp):
        pg_db = DBPostgres()
        sql_query = f"SELECT * FROM ipdr_loc WHERE phone = '{phone}' AND time_stamp = '{timestamp}'"
        pg_result = pg_db.execute(sql_query)
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        is_exist = False
        if len(rows) > 0:
            is_exist = True
        return is_exist

    def insert_into_db(self, values):
        pg_db = DBPostgres()
        sql_query = f"INSERT INTO ipdr_loc(time_stamp, source, cgi, village, city, province, country, geolocation, rat, sid, lac, ci_eci, outbound_roaming, mcc, mnc, operator, estimated_location, target_name, phone, upload_time) VALUES {values}"
        pg_result = pg_db.execute(sql_query)
        return pg_result

    def insert_target_tracing(self, target, phone):
        pg_db = DBPostgres()
        sql_query = f"INSERT INTO ipdr_loc_tracing (SELECT * FROM ipdr_loc WHERE time_stamp IN (SELECT MAX(time_stamp) as time_stamp FROM ipdr_loc WHERE target_name='{target}' AND phone='{phone}' AND city <> 'None' AND city <> '' AND province <> 'None' AND province <> '' GROUP BY city, province, country, target_name, phone, operator))"
        pg_result = pg_db.execute(sql_query)
        return pg_result

    def get_notification(self, phone, target, user_id: str):
        pg_db = DBPostgres()
        sql_query_get_current_location = f"select b.province from ipdr_loc_tracing b where b.phone = '{phone}' and TO_CHAR(b.upload_time, 'YYYY-MM-DD') < '2020-10-22' order by b.upload_time desc, b.time_stamp desc limit 1"
        ## sql_query_get_current_location = f"select b.province from ipdr_loc_tracing b where b.phone = '{phone}' and TO_CHAR(b.upload_time, 'YYYY-MM-DD') < TO_CHAR(current_date, 'YYYY-MM-DD') order by b.upload_time desc, b.time_stamp desc limit 1"
        pg_result_current_location = pg_db.execute(
            sql_query_get_current_location)
        pg_columns_current_location = pg_result_current_location.keys()
        rows_current = [dict(zip(pg_columns_current_location, row_current_location))
                        for row_current_location in pg_result_current_location]
        # print(rows_current)

        # sql_query = f"select a.province from ipdr_loc_tracing a where a.phone = '{phone}' and TO_CHAR(a.upload_time, 'YYYY-MM-DD') = TO_CHAR(current_date, 'YYYY-MM-DD') order by a.upload_time asc, a.time_stamp asc"
        sql_query = f"select a.province from ipdr_loc_tracing a where a.phone = '{phone}' and TO_CHAR(a.upload_time, 'YYYY-MM-DD') = '2020-10-22' order by a.upload_time asc, a.time_stamp asc"
        ## sql_query = f"select a.province from ipdr_loc_tracing a where a.phone = '{phone}' and TO_CHAR(a.upload_time, 'YYYY-MM-DD') = TO_CHAR(current_date, 'YYYY-MM-DD') order by a.upload_time asc, a.time_stamp asc"
        pg_result = pg_db.execute(sql_query)
        # print(sql_query)
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        # print("lokasi terakhir di" + str(rows_current))
        # print("dia ke" + str(rows))
        union = rows_current + rows
        # print("gabungan terakhir dan dia ke" + str(union))
        result = []
        moving = []
        for row in union:
            p = row['province']
            result.append(p)
        # print(result)
        for x in range(0, len(result)-1):
            if result[x] != result[x+1]:
                moving.append(result[x+1])
            # else:
            #     print('not match')
        # print("berpindah ke" + str(moving))
        return moving

    def get_notification_alert_management(self, phone, target, user_id: str):
        pg_db = DBPostgres()
        sql_query_geolocation = f"select a.geolocation from ipdr_loc_tracing a where a.phone = '{phone}' and TO_CHAR(a.upload_time, 'YYYY-MM-DD') = '2020-10-22' order by a.upload_time asc, a.time_stamp asc"
        # sql_query_geolocation = f"select a.geolocation from ipdr_loc_tracing a where a.phone = '{phone}' and TO_CHAR(a.upload_time, 'YYYY-MM-DD') = TO_CHAR(current_date, 'YYYY-MM-DD') order by a.upload_time asc, a.time_stamp asc"
        pg_result_geolocation = pg_db.execute(sql_query_geolocation)
        pg_columns_geolocation = pg_result_geolocation.keys()
        rows_geolocation_target = [
            dict(zip(pg_columns_geolocation, row)) for row in pg_result_geolocation]

        convert_geolocation = []
        result_inside = []
        result_outside = []
        data_identification_by_phone = list(self.identification_table.find(
            {"targets":
                {"$elemMatch": {"phone": phone}}
             }))
        # print(len(data_identification_by_phone))
        if len(data_identification_by_phone) > 0:
            for datas_identification_by_phone in data_identification_by_phone:
                id_identification = datas_identification_by_phone['_id']
                data_alert_management = list(self.alert_management_table.aggregate([
                    {'$match':  {
                        'identification_id': ObjectId(id_identification),
                        'deleted_by': None,
                        'status': 'Active'
                    }
                    },
                    {"$project": {
                        "_id": 1,
                        "identification_id": 2,
                        "name": 3,
                        "geolocation": 4,
                        "condition": 5
                    }
                    }
                ]))
                # print(len(data_alert_management))
                if len(data_alert_management) > 0:
                    for datas_alert_management in data_alert_management:
                        id_alert = datas_alert_management['_id']
                        alert_name = datas_alert_management['name']
                        id_identification_alert = datas_alert_management['identification_id']
                        condition = datas_alert_management['condition']
                        geolocation = datas_alert_management['geolocation']['features'][0]['geometry']['coordinates']

                        data_match_alert_identification = list(self.identification_table.aggregate([
                            {"$match": {
                                "_id": ObjectId(id_identification_alert),
                                "deleted_by": None
                            }
                            },
                            {"$project": {
                                "_id": 1,
                                "name": 2
                            }
                            }]))
                        if len(data_match_alert_identification) > 0:
                            #     print(len(data_match_alert_identification)
                            for datas_match_alert_identification in data_match_alert_identification:
                                identification_name = datas_match_alert_identification['name']
                            for geolocations in geolocation:
                                row_geolocation = []
                                for data_geolocations in geolocations:
                                    coordinate_geolocation = float(
                                        data_geolocations)
                                    row_geolocation.append(
                                        coordinate_geolocation)
                                convert_geolocation.append(row_geolocation)
                            # print(convert_geolocation)
                            for rows_geolocation_targets in rows_geolocation_target:
                                longlat = [float(rows_geolocation_targets['geolocation'].split(",")[0]), float(
                                    rows_geolocation_targets['geolocation'].split(",")[1])]
                                result = self.polygon_check(
                                    longlat, convert_geolocation)
                            # print(result)
                            if result == True:
                                result_inside.append(result)
                                # break
                                # print(result_inside)
                            else:
                                result_outside.append(result)
                                # break
                                # print(result_outside)
                        # print(result_inside)
                        # print(result_outside)

                            conditions = str(condition)
                            if conditions == "['Outside']":
                                if len(result_outside) > 0:
                                    message_outside = {
                                        'alert_id': id_alert,
                                        'alert_name': alert_name,
                                        'identification_id': id_identification_alert,
                                        'name': identification_name,
                                        'target': target,
                                        'phone': phone,
                                        'description': 'target ' + target + ' ' + phone + ' melakukan pergerakan keluar area polygon ' + identification_name,
                                        'status': 'unread',
                                        'created_by': user_id,
                                        'created_date': datetime.datetime.now(),
                                        'updated_by': None,
                                        'updated_date': None,
                                        'deleted_by': None,
                                        'deleted_date': None
                                    }
                                    self.notification_table.insert_one(
                                        message_outside).inserted_id
                                    print(message_outside)
                            elif conditions == "['Inside']":
                                if len(result_inside) > 0:
                                    message_inside = {
                                        'alert_id': id_alert,
                                        'alert_name': alert_name,
                                        'identification_id': id_identification_alert,
                                        'name': identification_name,
                                        'target': target,
                                        'phone': phone,
                                        'description': 'target ' + target + ' ' + phone + ' melakukan pergerakan kedalam area polygon ' + identification_name,
                                        'status': 'unread',
                                        'created_by': user_id,
                                        'created_date': datetime.datetime.now(),
                                        'updated_by': None,
                                        'updated_date': None,
                                        'deleted_by': None,
                                        'deleted_date': None
                                    }
                                    self.notification_table.insert_one(
                                        message_inside).inserted_id
                                    print(message_inside)
                            elif conditions == "['Inside', 'Outside']":
                                if len(result_inside) > 0 or len(result_outside) > 0:
                                    message_outside = {
                                        'alert_id': id_alert,
                                        'alert_name': alert_name,
                                        'identification_id': id_identification_alert,
                                        'name': identification_name,
                                        'target': target,
                                        'phone': phone,
                                        'description': 'target ' + target + ' ' + phone + ' melakukan pergerakan keluar area polygon ' + identification_name,
                                        'status': 'unread',
                                        'created_by': user_id,
                                        'created_date': datetime.datetime.now(),
                                        'updated_by': None,
                                        'updated_date': None,
                                        'deleted_by': None,
                                        'deleted_date': None
                                    }
                                    self.notification_table.insert_one(
                                        message_outside).inserted_id

                                    message_inside = {
                                        'alert_id': id_alert,
                                        'alert_name': alert_name,
                                        'identification_id': id_identification_alert,
                                        'name': identification_name,
                                        'target': target,
                                        'phone': phone,
                                        'description': 'target ' + target + ' ' + phone + ' melakukan pergerakan kedalam area polygon ' + identification_name,
                                        'status': 'unread',
                                        'created_by': user_id,
                                        'created_date': datetime.datetime.now(),
                                        'updated_by': None,
                                        'updated_date': None,
                                        'deleted_by': None,
                                        'deleted_date': None
                                    }
                                    self.notification_table.insert_one(
                                        message_inside).inserted_id
                                    print(message_inside)
                                    print(message_outside)
        return rows_geolocation_target

    def polygon_check(self, coordinate, dd_new):
        point = Feature(geometry=Point((coordinate)))
        polygon = Polygon(
            [
                dd_new
            ]
        )
        ax = boolean_point_in_polygon(point, polygon)
        return ax

    def geolocation(self, coordinates):
        geolocator = Nominatim(user_agent="TargetTracing1", timeout=2)
        location = geolocator.reverse(coordinates)
        address = location.raw['address']
        country = None
        province = None
        city = None
        village = None

        try:
            country = address['country']
        except Exception as exc:
            country = None

        try:
            province = address['state']
        except Exception as exc:
            province = None

        try:
            city = address['city']
        except Exception as exc:
            try:
                city = address['state_district']
            except Exception as exc:
                city = None

        try:
            village = address['village']
        except Exception as exc:
            village = None

        result = {
            'country': country,
            'province': province,
            'city': city,
            'village': village
        }
        return result

    def geolocation_all(self, coordinates):
        geolocator = Nominatim(user_agent="TargetTracing1", timeout=2)
        location = geolocator.reverse(coordinates)
        location_list = location.address.split(', ')
        country = location_list[-1]
        province = location_list[-2]
        city = location_list[-3]
        village = location_list[-4]
        result = {
            'country': country,
            'province': province,
            'city': city,
            'village': village
        }
        return result

    def target_location(self, target_name, phone):
        pg_db = DBPostgres()
        sql_query = f"SELECT * FROM ipdr_loc WHERE target_name = '{target_name}' AND phone = '{phone}' ORDER BY time_stamp DESC LIMIT 1"
        pg_result = pg_db.execute(sql_query)
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        row = None
        result = None
        if len(rows) > 0:
            row = rows[0]
            result = {
                'country': row['country'],
                'province': row['province'],
                'city': row['city'],
                'village': row['village']
            }
        return result

    def data(self, limit, offset, key, sorter):
        pg_result = DBPostgres.execute(
            text(f"SELECT time_stamp as date_time, target_name as name, phone, operator as provider, city, province, country FROM ipdr_loc WHERE TO_CHAR(time_stamp, 'yyyy-MM-dd HH:mm:ss') like '%{key}%' OR LOWER(source) like '%{key}%' OR LOWER(cgi) like '%{key}%' OR LOWER(country) like '%{key}%' OR LOWER(site_name) like '%{key}%' OR LOWER(cell_name) like '%{key}%' OR LOWER(geolocation) like '%{key}%' OR LOWER(rat) like '%{key}%' OR LOWER(sid) like '%{key}%' OR LOWER(lac) like '%{key}%' OR LOWER(ci_eci) like '%{key}%' OR LOWER(outbound_roaming) like '%{key}%' OR LOWER(mcc) like '%{key}%' OR LOWER(mnc) like '%{key}%' OR LOWER(operator) like '%{key}%' OR LOWER(estimated_location) like '%{key}%' OR LOWER(target_name) like '%{key}%' OR LOWER(village) like '%{key}%' OR LOWER(city) like '%{key}%' OR LOWER(province) like '%{key}%' OR LOWER(phone) like '%{key}%' ORDER BY time_stamp {limit}"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def data_count(self, key):
        pg_result = DBPostgres.execute(
            text(f"SELECT COUNT(*) as total FROM ipdr_loc WHERE TO_CHAR(time_stamp, 'yyyy-MM-dd HH:mm:ss') like '%{key}%' OR LOWER(source) like '%{key}%' OR LOWER(cgi) like '%{key}%' OR LOWER(country) like '%{key}%' OR LOWER(site_name) like '%{key}%' OR LOWER(cell_name) like '%{key}%' OR LOWER(geolocation) like '%{key}%' OR LOWER(rat) like '%{key}%' OR LOWER(sid) like '%{key}%' OR LOWER(lac) like '%{key}%' OR LOWER(ci_eci) like '%{key}%' OR LOWER(outbound_roaming) like '%{key}%' OR LOWER(mcc) like '%{key}%' OR LOWER(mnc) like '%{key}%' OR LOWER(operator) like '%{key}%' OR LOWER(estimated_location) like '%{key}%' OR LOWER(target_name) like '%{key}%' OR LOWER(village) like '%{key}%' OR LOWER(city) like '%{key}%' OR LOWER(province) like '%{key}%' OR LOWER(phone) like '%{key}%'"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        result = None
        if len(rows) > 0:
            result = rows[0]
            result = result['total']
        return result

    def phones(self):
        pg_result = DBPostgres.execute(
            text(f"SELECT distinct phone FROM ipdr_loc"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        result = []
        for row in rows:
            p = row['phone']
            result.append(p)
        return result

    def ipdr_loc_trx(self, param):
        pg_result = DBPostgres.execute(
            text(f"SELECT TO_CHAR(time_stamp,'YYYY-MM-DD hh:mm:ss') as date_time, target_name as name, phone, operator as provider, village, city, province, country, source, cgi, geolocation, rat, sid, lac, ci_eci, outbound_roaming, mcc, mnc FROM ipdr_loc {param} ORDER BY time_stamp ASC"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def ipdr_loc_trx_2(self, param):
        pg_result = DBPostgres.execute(
            text(f'''SELECT lvl1.max_time_stamp as time_stamp, lvl1.name, lvl1.phone, lvl1.geolocation, CASE WHEN lvl1.total_trx > 1 THEN info ELSE CONCAT('', max_time_stamp) END as info, lvl2.operator as provider, 
                    lvl2.village, lvl2.city, lvl2.province, lvl2.country, lvl2.source, lvl2.cgi, lvl2.geolocation, lvl2.rat, lvl2.sid, lvl2.lac, lvl2.ci_eci, 
                    lvl2.outbound_roaming, lvl2.mcc, lvl2.mnc--, CASE WHEN lvl1.total_trx > 1 THEN info ELSE CONCAT('Date: ', max_time_stamp) END as info2
                    FROM (SELECT MAX(time_stamp) as max_time_stamp
                    , target_name as name, phone, geolocation, CONCAT('From: ', MIN(time_stamp), ', To: ', MAX(time_stamp)) as info, COUNT(geolocation) as total_trx FROM ipdr_loc 
                    {param}
                    GROUP BY geolocation, target_name, phone) as lvl1
                    JOIN ipdr_loc as lvl2 ON lvl2.time_stamp = lvl1.max_time_stamp 
                    AND lvl2.phone = lvl1.phone AND lvl2.target_name = lvl1.name 
                    AND lvl2.geolocation = lvl1.geolocation
                    ORDER BY lvl1.phone, lvl1.max_time_stamp'''))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def get_mobility(self, date_time_from, date_time_to, limit=20, offset=0, phones=()):
        list_phones = list(phones)
        where_phones = ''
        query = ''
        if len(list_phones) <= 0:
            query = f"select phone,target_name as name, count(*) as total from ipdr_loc group by phone, name order by phone, total desc offset {offset} limit {limit}"
            if date_time_from != '' and date_time_to != '':
                query = f"select phone,target_name as name, count(*) as total from ipdr_loc  where time_stamp between '{date_time_from}' and '{date_time_to}' group by phone,name order by phone, total desc offset {offset} limit {limit}"

        elif len(list_phones) == 1:
            where_phones = f'{list_phones[0]}'
            query = f"select phone,target_name as name, count(*) as total from ipdr_loc  where phone = '{where_phones}' group by phone, name order by phone, total desc offset {offset} limit {limit}"
            if date_time_from != '' and date_time_to != '':
                query = f"select phone,target_name as name, count(*) as total from ipdr_loc  where phone = '{where_phones}' and time_stamp between '{date_time_from}' and '{date_time_to}' group by phone,name order by phone, total desc offset {offset} limit {limit}"

        elif len(list_phones) > 1:
            query = f"select phone,target_name as name, count(*) as total from ipdr_loc  where phone in {phones} group by phone, name order by phone, total desc offset {offset} limit {limit}"
            if date_time_from != '' and date_time_to != '':
                query = f"select phone,target_name as name, count(*) as total from ipdr_loc  where phone in {phones} and time_stamp between '{date_time_from}' and '{date_time_to}' group by phone,name order by phone, total desc offset {offset} limit {limit}"
        query = query.replace(",)", ")")

        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def get_mobility_distances(self, phone, date_time_from, date_time_to, limit=2000, offset=0):
        query = text(
            f"with record as ( select distinct on (geolocation) geolocation,village,city,province, time_stamp as date_time, target_name as name, phone from ipdr_loc  where phone = '{phone}' group by phone,name, geolocation, village, city, province, date_time order by geolocation, phone, date_time desc),co as (select count(*) as total from record where record.geolocation <> '') select record.*, co.* from record, co where record.geolocation <> ''  order by phone asc, name asc, date_time asc , geolocation asc offset {offset} limit {limit}")
        if date_time_from != '' and date_time_to != '':
            query = text(
                f"with record as ( select distinct on (geolocation) geolocation,village,city,province, time_stamp as date_time, target_name as name, phone from ipdr_loc  where phone = '{phone}' and time_stamp between '{date_time_from}' and '{date_time_to}' group by phone,name, geolocation, village, city, province, date_time order by geolocation, phone, date_time desc),co as (select count(*) as total from record where record.geolocation <> '') select record.*, co.* from record, co where record.geolocation <> ''  order by phone asc, name asc, date_time asc , geolocation asc offset {offset} limit {limit}")

        pg_result = DBPostgres.execute(query)
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def get_total_last_update(self, phones: tuple):
        query = f'select distinct on (phone) phone, time_stamp as date_time, target_name as name, phone from ipdr_loc group by phone,name, geolocation, village, city, date_time order by phone, date_time desc'
        if len(list(phones)) == 1:
            where_phone = list(phones)[0]
            query = f"select distinct on (phone) phone, time_stamp as date_time, target_name as name, phone from ipdr_loc  where phone = '{where_phone}' group by phone,name, geolocation, village, city, date_time order by phone, date_time desc"

        elif len(list(phones)) > 1:
            query = f'select distinct on (phone) phone, time_stamp as date_time, target_name as name, phone from ipdr_loc  where phone in {phones} group by phone,name, geolocation, village, city, date_time order by phone, date_time desc'

        pg_result = DBPostgres.execute(
            text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def get_target_phone(self, limit, offset, key, sorter):
        pg_result = DBPostgres.execute(
            text(f"select distinct target_name as name, phone from ipdr_loc WHERE LOWER(phone) LIKE '%{key}%' OR LOWER(target_name) LIKE '%{key}%' ORDER BY target_name, phone {limit}"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def update_ipdr_loc(self, phone_old, phone_new, name_old, name_new):
        pg_db = DBPostgres()
        sql_query = f"UPDATE ipdr_loc SET phone='{phone_new}', target_name='{name_new}' WHERE phone='{phone_old}' AND target_name='{name_old}'"
        pg_db.execute(sql_query)

    def update_ipdr_loc_tracing(self, phone_old, phone_new, name_old, name_new):
        pg_db = DBPostgres()
        sql_query = f"UPDATE ipdr_loc_tracing SET phone='{phone_new}', target_name='{name_new}' WHERE phone='{phone_old}' AND target_name='{name_old}'"
        pg_db.execute(sql_query)

    def delete_ipdr_loc(self, phone, name):
        pg_db = DBPostgres()
        sql_query = f"DELETE FROM ipdr_loc WHERE phone='{phone}' AND target_name='{name}'"
        pg_db.execute(sql_query)

    def delete_ipdr_loc_tracing(self, phone, name):
        pg_db = DBPostgres()
        sql_query = f"DELETE FROM ipdr_loc_tracing WHERE phone='{phone}' AND target_name='{name}'"
        pg_db.execute(sql_query)

    def delete_target_on_identification(self, phone, name):
        pg_db = DBPostgres()
        sql_query = f"DELETE FROM ipdr_loc_tracing WHERE phone='{phone}' AND target_name='{name}'"
        pg_db.execute(sql_query)

    def data_target_phone_count(self, key):
        pg_result = DBPostgres.execute(
            text(f"select distinct phone, target_name as target from ipdr_loc WHERE LOWER(phone) LIKE '%{key}%' OR LOWER(target_name) LIKE '%{key}%'"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        result = len(rows)
        return result

    def insert_identification(self, model: InsertIdentificationModel):
        data = model.__dict__
        self.identification_table.insert_one(data)

    def update_identification(self, model: UpdateIdentificationModel):
        self.identification_table.update({'_id': model.id}, {
            '$set': {
                'name': model.name,
                'date': model.date,
                'description': model.description,
                'targets': model.targets,
                'updated_by': model.updated_by,
                'updated_date': model.updated_date
            }}, multi=True)

    def delete_identification(self, model: UpdateIdentificationModel):
        self.identification_table.update({'_id': model.id}, {
            '$set': {
                'deleted_by': model.deleted_by,
                'deleted_date': model.deleted_date
            }}, multi=True)

    def data_identification(self, limit, offset, key, sort):
        data = list(self.identification_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'date': {"$regex": key, "$options": "i"}
            }, {
                'description': {"$regex": key, "$options": "i"}
            }, {
                'targets.name': {"$regex": key, "$options": "i"}
            }, {
                'targets.phone': {"$regex": key, "$options": "i"}
            }]
        }).sort([('created_date', -1)]).skip(offset).limit(limit))
        return data

    def data_identification_count(self, key):
        data = list(self.identification_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'date': {"$regex": key, "$options": "i"}
            }, {
                'description': {"$regex": key, "$options": "i"}
            }, {
                'targets.name': {"$regex": key, "$options": "i"}
            }, {
                'targets.phone': {"$regex": key, "$options": "i"}
            }]
        }))
        result = len(data)
        return result

    def data_identification_targets(self, key):
        pg_result = DBPostgres.execute(
            text(f"select CONCAT(t.name, '-', t.phone) as value, CONCAT(t.name, ' - ', t.phone) as text from (select distinct target_name as name, phone from ipdr_loc WHERE LOWER(phone) LIKE '%{key}%' OR LOWER(target_name) LIKE '%{key}%') as t ORDER BY t.name limit 10"))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def data_identification_by_name(self, name):
        data = list(self.identification_table.find({
            'deleted_by': None,
            'name': name
        }))
        return data

    def data_identification_by_id(self, key):
        if key == '':
            return {'targets': []}
        data = self.identification_table.find_one({
            'deleted_by': None,
            '_id': ObjectId(key)
        })
        return data

    def data_identification_by_name_regex(self, key, limit):
        data = list(self.identification_table.find({
            'deleted_by': None,
            'name': {"$regex": key, "$options": "i"}
        }, {'name': 1}).sort([('created_date', -1)]).limit(limit))
        return data

    def data_identification_by_target(self, target):
        data = list(self.identification_table.aggregate([
            {
                '$match': {
                    'targets': {
                        '$in': [{
                            'name': target['name'],
                            'phone': target['phone']
                        }]
                    }
                }
            }
        ]))
        return data

    def data_pie_chart(self, params, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
            SELECT is_moving, COUNT(is_moving) FROM
            (SELECT phone, date, COUNT(geolocation) as total, CASE WHEN COUNT(geolocation) = 1 THEN false ELSE true END as is_moving FROM 
            (SELECT distinct geolocation, TO_CHAR(time_stamp, 'YYYY-MM-DD') as date, phone FROM ipdr_loc 
            {params}
            AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
            ORDER BY phone, TO_CHAR(time_stamp, 'YYYY-MM-DD')
            ) AS h 	
            GROUP BY phone, date) AS j
            GROUP BY is_moving
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def data_bottom_bar_chart(self, params, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT province, is_moving, COUNT(is_moving) FROM
                (SELECT phone, province, COUNT(geolocation) as total, CASE WHEN COUNT(geolocation) = 1 THEN false ELSE true END as is_moving FROM (
                SELECT distinct geolocation, TO_CHAR(time_stamp, 'YYYY-MM-DD') as date, phone, province FROM ipdr_loc 
                {params}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                ORDER BY phone, TO_CHAR(time_stamp, 'YYYY-MM-DD')
                ) AS h
                GROUP BY phone, province)AS j
                GROUP BY province, is_moving
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def insert_target(self, model: InsertTargetModel):
        data = model.__dict__
        self.tracing_target_table.insert_one(data)

    def update_target(self, model: UpdateTargetModel):
        self.tracing_target_table.update({'_id': model.id}, {
            '$set': {
                'phone': model.phone,
                'name': model.name,
                'updated_by': model.updated_by,
                'updated_date': model.updated_date
            }}, multi=True)

    def delete_target(self, model: UpdateIdentificationModel):
        self.tracing_target_table.update({'_id': model.id}, {
            '$set': {
                'deleted_by': model.deleted_by,
                'deleted_date': model.deleted_date
            }}, multi=True)

    def data_target(self, limit, offset, key, sort):
        data = list(self.tracing_target_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'phone': {"$regex": key, "$options": "i"}
            }]
        }).sort([('created_date', -1)]).skip(offset).limit(limit))
        return data

    def data_target_count(self, key):
        data = list(self.tracing_target_table.find({
            'deleted_by': None,
            '$or': [{
                'name': {"$regex": key, "$options": "i"}
            }, {
                'phone': {"$regex": key, "$options": "i"}
            }]
        }))
        result = len(data)
        return result

    def data_target_by_id(self, key):
        data = self.tracing_target_table.find_one({
            'deleted_by': None,
            '_id': key
        })
        return data

    def data_detail_pie_chart(self, params, awal, akhir, status):
        pg_result = DBPostgres.execute(
            text(f'''
                    SELECT * from
                    (SELECT target_name ,date, phone, province, COUNT(geolocation) as total, CASE WHEN COUNT(geolocation) = 1 THEN 'stay' ELSE 'moving' END AS is_moving FROM (
                    SELECT distinct geolocation, target_name, province, TO_CHAR(time_stamp, 'YYYY-MM-DD') as date, phone FROM ipdr_loc 
                    {params}
                    AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                    ORDER BY phone, TO_CHAR(time_stamp, 'YYYY-MM-DD')
                    ) AS h
                    GROUP BY phone, date, target_name, province) AS j
                    WHERE is_moving='{status}'
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def get_provider_name(self, phone):
        query = f"select operator  from ipdr_loc where phone = '{phone}' limit 1"
        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def detail_top_ten(self, phone, date_time_from, date_time_to):
        if not phone:
            return []
        where_time = ''
        if date_time_from and date_time_to:
            where_time = f"and time_stamp between '{date_time_from}' and '{date_time_to}' "
        query = f"with list as(select time_stamp,phone,geolocation, substring(time_stamp::text, 0, 11) as date, case when ( geolocation = lag(geolocation) over (order by time_stamp asc) or  geolocation = lead(geolocation) over (order by time_stamp asc)) then 'y' else 'n' end  same from ipdr_loc  where phone = '{phone}' {where_time}), same_group as (select * from list where same = 'y'), get_first_time as (select *, first_value(time_stamp) over (partition by date, geolocation order by time_stamp asc) as first_time from same_group), get_diff as (select *, extract(epoch from time_stamp - first_time) as diff from get_first_time), filter_stay_half_hour as ( select * from get_diff where diff > 1800 order by time_stamp asc), final_result as (select  distinct on(first_time) first_time, phone, time_stamp, geolocation, diff, date from filter_stay_half_hour order by first_time, time_stamp asc), percentage as (select geolocation, count(*)  as count from final_result group by geolocation order by count desc) select *, round((count*100/ sum(count) over())::numeric,2) as percentage from percentage limit 10"
        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def detail_address_analysis(self, phone, date_time_from, date_time_to):
        if not phone:
            return []
        where_time = ''
        if date_time_from and date_time_to:
            where_time = f"and time_stamp between '{date_time_from}' and '{date_time_to}' "
        query = f"with list as(select time_stamp,phone,geolocation, substring(time_stamp::text, 0, 11) as date, substring(time_stamp::text, 12, 19) as time, case when ( geolocation = lag(geolocation) over (order by time_stamp asc) or  geolocation = lead(geolocation) over (order by time_stamp asc)) then 'y' else 'n' end  same from ipdr_loc  where phone = '{phone}' {where_time}), same_group as (select * from list where same = 'y'), get_first_time as (select *, first_value(time_stamp) over (partition by date, geolocation order by time_stamp asc) as first_time from same_group), get_diff as (select *, extract(epoch from time_stamp - first_time) as diff from get_first_time), filter_stay_half_hour as ( select * from get_diff where diff > 1800 order by time_stamp asc), final_result as (select  distinct on(first_time) first_time, phone, time_stamp, geolocation, diff, date, time from filter_stay_half_hour where (time > '22:00:00'  or time < '05:00:00')  and diff > 1800 order by first_time, time_stamp asc), percentage as (select geolocation, count(*)  as count from final_result group by geolocation order by count desc) select *, round((count*100/ sum(count) over()),2) as percentage from percentage limit 1"
        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def detail_office_analysis(self, phone, date_time_from, date_time_to):
        if not phone:
            return []
        where_time = ''
        if date_time_from and date_time_to:
            where_time = f"and time_stamp between '{date_time_from}' and '{date_time_to}' "
        query = f"with list as(select time_stamp,phone,geolocation, substring(time_stamp::text, 0, 11) as date, substring(time_stamp::text, 12, 19) as time, case when ( geolocation = lag(geolocation) over (order by time_stamp asc) or  geolocation = lead(geolocation) over (order by time_stamp asc)) then 'y' else 'n' end  same from ipdr_loc  where phone = '{phone}' {where_time}), same_group as (select * from list where same = 'y'), get_first_time as (select *, first_value(time_stamp) over (partition by date, geolocation order by time_stamp asc) as first_time from same_group), get_diff as (select *, extract(epoch from time_stamp - first_time) as diff from get_first_time), filter_stay_half_hour as ( select * from get_diff where diff > 1800 order by time_stamp asc), final_result as (select  distinct on(first_time) first_time, phone, time_stamp, geolocation, diff, date, time from filter_stay_half_hour where (time > '07:00:00'  and time < '17:00:00')  and diff > 1800 order by first_time, time_stamp asc), percentage as (select geolocation, count(*)  as count from final_result group by geolocation order by count desc) select *, round((count*100/ sum(count) over()),2) as percentage from percentage limit 1"
        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def detail_records(self, phone, offset=0, limit=10):
        if not phone:
            return []
        query = f"select geolocation, mcc, mnc, lac, ci_eci as cell_id, province, city, village, time_stamp from ipdr_loc where phone = '{phone}' order by time_stamp desc offset {offset} limit {limit}"
        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        return rows

    def detail_records_count(self, phone):
        if not phone:
            return []
        query = f"select count (*) from ipdr_loc where phone = '{phone}'"
        pg_result = DBPostgres.execute(text(query))
        pg_columns = pg_result.keys()
        rows = [dict(zip(pg_columns, row)) for row in pg_result]
        count = 0
        if len(rows) > 0:
            count = rows[0]['count']
        return count

    def data_filter_map_date(self, params, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
            SELECT phone AS phone_number, MIN(TO_CHAR(time_stamp,'YYYY-MM-DD')) AS start_date  FROM ipdr_loc
            {params}
            AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
            GROUP BY phone_number
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def data_bottom_table_map(self, param):
        pg_result = DBPostgres.execute(
            text(f'''
            SELECT target_name as target, phone as phone_number, MIN(TO_CHAR(time_stamp,'YYYY-MM-DD hh:mm:ss')) as start_date, MAX(TO_CHAR(time_stamp,'YYYY-MM-DD hh:mm:ss')) as end_date, COUNT(geolocation) as count_record FROM ipdr_loc 
            {param}
            GROUP BY target, phone
            '''))
        print(pg_result)
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def data_detail_tracing(self, param):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT time_stamp, geolocation, province, city, village
                FROM ipdr_loc
                {param}
            '''))
        print(pg_result)
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def add_name(self, params, filter_by, awal, akhir, position):
        pg_result = DBPostgres.execute(
            text(f'''
                    SELECT first.target_name AS name, is_moving FROM
                    (SELECT distinct target_name, MAX(time_stamp) AS finish, COUNT(distinct({filter_by})) as total, CASE WHEN COUNT(distinct({filter_by})) = 1 THEN false ELSE true END as is_moving FROM ipdr_loc 
                    {params}
                    AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                    GROUP BY target_name ORDER BY target_name, finish) AS first
                    INNER JOIN
                    (SELECT distinct target_name, MAX(time_stamp) AS finish, {filter_by} FROM ipdr_loc 
                    {params}
                    AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                    GROUP BY target_name, {filter_by} ORDER BY target_name) AS second
                    ON first.finish = second.finish
                    WHERE {filter_by} = '{position}'
                    GROUP BY name, is_moving
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def new_data_pie_chart(self, params, filter_by, by_filter, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
                    SELECT is_moving, COUNT(is_moving) FROM
                    (SELECT phone, COUNT({filter_by}) as total, CASE WHEN COUNT({filter_by}) = 1 THEN false ELSE true END as is_moving FROM (
                    SELECT distinct {by_filter} phone FROM ipdr_loc 
                    {params}
                    AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                    ORDER BY phone
                    ) AS h
                    GROUP BY phone)AS j
                    GROUP BY is_moving
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def new_bar_chart(self, params, filter_by, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT is_moving, {filter_by}, COUNT({filter_by}) FROM
                (SELECT distinct target_name, MAX(time_stamp) AS finish, COUNT(distinct({filter_by})) as total, CASE WHEN COUNT(distinct({filter_by})) = 1 THEN false ELSE true END as is_moving FROM ipdr_loc 
                {params}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                GROUP BY target_name ORDER BY target_name, finish) AS first
                INNER JOIN
                (SELECT distinct target_name, MAX(time_stamp) AS finish, {filter_by} FROM ipdr_loc 
                {params}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                GROUP BY target_name, {filter_by} ORDER BY target_name) AS second
                ON first.finish = second.finish
                GROUP BY is_moving, {filter_by}
                ORDER BY {filter_by}
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def new_detail_pie_chart(self, param, awal, akhir, filter_by, status, limit):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT * FROM
                (SELECT first.target_name AS name, phone, {filter_by}, total, is_moving FROM
                (SELECT distinct target_name, phone, MAX(time_stamp) AS finish, COUNT(distinct({filter_by})) as total, CASE WHEN COUNT(distinct({filter_by})) = 1 THEN 'stay' ELSE 'moving' END as is_moving FROM ipdr_loc 
                {param}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                GROUP BY target_name, phone ORDER BY target_name, finish) AS first
                INNER JOIN
                (SELECT distinct target_name, MAX(time_stamp) AS finish, {filter_by} FROM ipdr_loc 
                {param}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
                GROUP BY target_name, {filter_by} ORDER BY target_name) AS second
                ON first.finish = second.finish
                GROUP BY name, phone, {filter_by}, is_moving, total) AS third
                WHERE is_moving in ('{status}')
                {limit}
                '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def top_ten_location(self, params, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT province, city, village, geolocation FROM
                (SELECT * FROM
                (SELECT MIN(time_stamp), MAX(time_stamp),phone,geolocation, province, city, village, date, EXTRACT(epoch FROM MAX(time_stamp) - MIN(time_stamp)) as diff FROM
                (SELECT * FROM
                (SELECT time_stamp, phone, geolocation, province, city, village, SUBSTRING(time_stamp::text, 0, 11) AS date, CASE WHEN(geolocation=LAG(geolocation) OVER (ORDER BY time_stamp ASC) OR geolocation=LEAD(geolocation) OVER (ORDER BY time_stamp ASC)) THEN 'y' ELSE 'n' END AS same
                FROM ipdr_loc
                {params}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}') AS first_layer
                WHERE same = 'y') AS second_layer
                GROUP BY phone,geolocation,date,province, city, village ORDER BY date) AS third_layer
                WHERE diff>1800 ORDER BY diff DESC) AS fourth_layer
                GROUP BY geolocation, province, city, village ORDER BY SUM(diff) DESC
                LIMIT 10
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def another_phone(self, param_other, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT DISTINCT phone FROM ipdr_loc
                WHERE phone NOT IN {param_other}
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result

    def get_name_by_phone(self, phone, awal, akhir):
        pg_result = DBPostgres.execute(
            text(f'''
                SELECT DISTINCT target_name AS name FROM ipdr_loc
                WHERE phone='{phone}'
                AND TO_CHAR(time_stamp, 'YYYY-MM-DD') BETWEEN '{awal}' AND '{akhir}'
            '''))
        pg_columns = pg_result.keys()
        result = [dict(zip(pg_columns, row)) for row in pg_result]
        return result
