import pandas
import json
import logging
from datetime import datetime, timedelta
import pymongo
import hashlib
import os
import pandas as pd
import csv
from apis.db_pg_api import DBPostgres
from sqlalchemy.sql import text
from configs.env_config import ENV_CONFIG
from apis.db_mongo_api import main_mongo_db


class TaskQuery():
    laporan_collection = main_mongo_db['laporan']

    def waiting_datas(self):
        pg_db = DBPostgres()
        sql_query = f"select task_assignment_nid, start_date, end_date, task_nid, task_status_pid from task_assignment_tt where CURRENT_DATE > end_date and task_status_pid='12001' or task_status_pid='12002'"
        pg_result = pg_db.execute(sql_query)
        pg_columns = pg_result.keys()
        datas = [dict(zip(pg_columns, row)) for row in pg_result]
        result: list = []
        if len(datas) > 0:
            for data in datas:
                result.append(data)
        return result

    def update_overdue_number(self, task_id: str,):
        pg_db = DBPostgres()
        sql_query = f"UPDATE task_assignment_tt SET task_status_pid='12006', color='#f1c40f' WHERE task_assignment_nid='{task_id}'"
        pg_result = pg_db.execute(sql_query)
        return pg_result
