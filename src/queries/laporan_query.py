import pandas
import json
import logging
from datetime import datetime, timedelta
import pymongo
import hashlib
import os
import pandas as pd
import csv
from apis.db_pg_api import DBPostgres
from sqlalchemy.sql import text
from configs.env_config import ENV_CONFIG
from collections import defaultdict
from apis.db_mongo_api import main_mongo_db
from bson.objectid import ObjectId


class LaporanQuery():
    laporan_collection = main_mongo_db['laporan']

    def waiting_datas(self):
        datas = list(self.laporan_collection.find(
            {'status': 'menunggu', 'overhandle': 0, 'deleted_date': None}))
        result: list = []
        for data in datas:
            datetime_now = (datetime.now()).day
            created_date = (data['created_date']).day
            x = datetime_now-created_date
            if x > 0:
                result.append(data)
        return result

    def verification_datas(self):
        datas = list(self.laporan_collection.find(
            {'status': 'verifikasi', 'overhandle': 0, 'deleted_date': None, 'accepted_date': None}))
        result: list = []
        for data in datas:
            datetime_now = (datetime.now()).day
            verifikasi_date = (data['verifikasi_date']).day
            x = datetime_now-verifikasi_date
            if x > 0:
                result.append(data)
        return result

    def data_by_id(self, id):
        data = self.laporan_collection.find_one(
            {'_id': id})
        return data

    def update_overhandle_number(self, id):
        self.laporan_collection.update({'_id': id}, {
            '$set': {
                'overhandle': 1
            }}, multi=True)
