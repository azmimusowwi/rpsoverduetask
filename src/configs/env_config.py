from os import getenv

from dotenv import load_dotenv

load_dotenv()

ENV_CONFIG = {
    'app': {
        'host': getenv('APP_HOST') or '127.0.0.1',
        'port': getenv('APP_PORT') or '5000',
        'host_prod': getenv('APP_HOST_PROD') or '127.0.0.1',
        'jwtSecret': getenv('APP_JWTSECRET') or 'jwtsecret',
    },
    'is_delete_file': getenv('IS_DELETE_FILE') or 'false',
    'file': {
        'upload_dir': getenv('FILE_UPLOAD_DIR') or '/tmp/',
        'media_upload_dir': getenv('FILE_MEDIA_UPLOAD_DIR') or '/tmp/',
        'media_upload_url': getenv('FILE_MEDIA_UPLOAD_URL') or 'http://localhost/uploads/',
        'laptip_upload_dir': getenv('FILE_LAPTIP_UPLOAD_DIR') or '/tmp/',
    },
    'file_sharing': {
        'current_dir': getenv('CURRENT_DIR_PATH') or '',
        'destination_dir': getenv('DESTINATION_DIR_PATH') or '',
    },
    'schedule': {
        'estimation': getenv('SCHEDULE_ESTIMATION') or ''
    },
    'db_pg': {
        'host': getenv('DB_PG_HOST') or 'localhost',
        'port': getenv('DB_PG_PORT') or '5432',
        'user': getenv('DB_PG_USER') or 'user',
        'password': getenv('DB_PG_PASSWORD') or 'pass',
        'database': getenv('DB_PG_DATABASE') or 'test',
    },
    'db_neo4j': {
        'host': getenv('DB_NEO4J_HOST') or 'localhost',
        'port': getenv('DB_NEO4J_PORT') or '7687',
        'user': getenv('DB_NEO4J_USER') or 'user',
        'password': getenv('DB_NEO4J_PASSWORD') or 'pass',
        'endpoint': getenv('DB_NEO4J_END_POINT') or 'http://server1.db2.giboxdigital.com:7474/db/data/transaction/commit',
        'auth': getenv('DB_NEO4J_AUTH')
    },
    'db_mongo': {
        'host': getenv('DB_MONGO_HOST') or 'localhost',
        'port': int(getenv('DB_MONGO_PORT') or 27017),
        'username': getenv('DB_MONGO_USERNAME') or '',
        'password': getenv('DB_MONGO_PASSWORD') or '',
        'database': getenv('DB_MONGO_DATABASE') or 'test',
    },
    'db_mongo_userman': {
        'host': getenv('DB_MONGO_USERMAN_HOST') or 'localhost',
        'port': int(getenv('DB_MONGO_USERMAN_PORT') or 27017),
        'username': getenv('DB_MONGO_USERMAN_USERNAME') or '',
        'password': getenv('DB_MONGO_USERMAN_PASSWORD') or '',
        'database': getenv('DB_MONGO_USERMAN_DATABASE') or 'test',
    },
    'db_hbase': {
        'host': getenv('DB_HBASE_HOST') or 'localhost',
        'port': int(getenv('DB_HBASE_PORT') or 9090),
    },
    'db_solr': {
        'url': getenv('DB_SOLR_URL') or 'http://localhost:8983',
    },
    'api_usermanagement': {
        'url': getenv('API_USERMANAGEMENT_URL') or 'http://localhost',
        'application_token': getenv('API_USERMANAGEMENT_APPLICATION_TOKEN') or '',
    },
    'api_licenseplate': {
        'host': getenv('API_LICENSEPLATE_HOST') or 'localhost',
        'port': getenv('API_LICENSEPLATE_PORT') or '50051',
    },
    'api_ner': {
        'host': getenv('API_NER_HOST') or 'localhost',
        'port': getenv('API_NER_PORT') or '50052',
    },
    'api_documentconverter': {
        'host': getenv('API_DOCUMENT_CONVERTER_HOST') or 'localhost',
        'port': getenv('API_DOCUMENT_CONVERTER_PORT') or '50054',
    },
    'api_phone': {
        'url': getenv('API_PHONE') or 'http://localhost'
    },
    'api_consolidate': {
        'url': getenv('API_CONSOLIDATE') or 'http://localhost',
        'url_textsearch': getenv('API_CONSOLIDATE_TEXTSEARCH') or 'http://localhost',
    },
    'dashboard_report': {
        'token': getenv('DASHBOARD_REPORT_TOKEN'),
        'url': getenv('DASHBOARD_REPORT_URL')
    },
    'api_hive': {
        'token': getenv('DB_HIVE_TOKEN'),
        'url': getenv('DB_HIVE_URL')
    },
    'api_ktp_dkcpl': {
        'url': getenv('API_EKTP_URL'),
        'user_name': getenv('API_EKTP_USERNAME'),
        'password': getenv('API_EKTP_PASSWORD')
    }
}
