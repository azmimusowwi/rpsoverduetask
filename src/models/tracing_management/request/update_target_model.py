from bson.objectid import ObjectId
from datetime import datetime


class UpdateTargetModel:
    def __init__(
            self,
            phone: str = '',
            name: str = '',
            nik: str = None,
            status: str = None,
            identity: dict = None,
            created_by: ObjectId = None,
            created_date: datetime = None,
            updated_by: ObjectId = None,
            updated_date: datetime = None,
            deleted_by: ObjectId = None,
            deleted_date: datetime = None,
            **kwargs
    ):
        self.phone = phone
        self.name = name
        self.nik = nik
        self.status = status
        self.identity = identity
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date
        self.deleted_by = deleted_by
        self.deleted_date = deleted_date
