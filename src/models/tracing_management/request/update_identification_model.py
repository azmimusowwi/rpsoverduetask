from bson.objectid import ObjectId
from typing import List
from datetime import datetime


class UpdateIdentificationModel:
    def __init__(
            self,
            id: ObjectId = None,
            name: str = '',
            date: str = '',
            description: str = '',
            targets: list = [],
            period_start: datetime = None,
            period_end: datetime = None,
            created_by: ObjectId = None,
            created_date: datetime = None,
            updated_by: ObjectId = None,
            updated_date: datetime = None,
            deleted_by: ObjectId = None,
            deleted_date: datetime = None,
            user_read: List[ObjectId] = [],
            user_write: List[ObjectId] = [],
            group_read: List[ObjectId] = [],
            group_write: List[ObjectId] = [],
            **kwargs
    ):
        self.id = id
        self.name = name
        self.date = date
        self.description = description
        self.period_start = period_start
        self.period_end = period_end
        self.targets = targets
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date
        self.deleted_by = deleted_by
        self.deleted_date = deleted_date
        self.user_read = user_read
        self.user_write = user_write
        self.group_read = group_read
        self.group_write = group_write

    @property
    def user_read(self):
        return self._user_read

    @user_read.setter
    def user_read(self, value):
        self._user_read = [ObjectId(oid_str) for oid_str in value]

    @property
    def user_write(self):
        return self._user_write

    @user_write.setter
    def user_write(self, value):
        self._user_write = [ObjectId(oid_str) for oid_str in value]

    @property
    def group_read(self):
        return self._group_read

    @group_read.setter
    def group_read(self, value):
        self._group_read = [ObjectId(oid_str) for oid_str in value]

    @property
    def group_write(self):
        return self._group_write

    @group_write.setter
    def group_write(self, value):
        self._group_write = [ObjectId(oid_str) for oid_str in value]
