from bson.objectid import ObjectId
from typing import List
from datetime import datetime


class IdentificationModel:
    def __init__(
            self,
            _id: ObjectId = None,
            name: str = '',
            date: str = '',
            description: str = '',
            period_start: datetime = None,
            period_end: datetime = None,
            targets: list = [],
            created_by: str = '',
            created_date: str = '',
            updated_by: str = '',
            updated_date: str = '',
            deleted_by: str = '',
            deleted_date: str = '',
            permission_id: str = '',
            user_read: List[ObjectId] = [],
            user_read_deny: List[ObjectId] = [],
            user_write: List[ObjectId] = [],
            user_write_deny: List[ObjectId] = [],
            group_read: List[ObjectId] = [],
            group_write: List[ObjectId] = [],
            **kwargs
    ):
        self._id = _id
        self.name = name
        self.date = date
        self.description = description
        self.period_start = period_start
        self.period_end = period_end
        self.targets = targets
        self.created_by = created_by
        self.created_date = created_date
        self.updated_by = updated_by
        self.updated_date = updated_date
        self.deleted_by = deleted_by
        self.deleted_date = deleted_date
        self.permission_id = permission_id
        self.user_read = user_read
        self.user_read_deny = user_read_deny
        self.user_write = user_write
        self.user_write_deny = user_write_deny
        self.group_read = group_read
        self.group_write = group_write

    @property
    def user_read(self):
        return self._user_read

    @user_read.setter
    def user_read(self, value):
        self._user_read = [ObjectId(oid_str) for oid_str in value]

    @property
    def user_read_deny(self):
        return self._user_read_deny

    @user_read_deny.setter
    def user_read_deny(self, value):
        self._user_read_deny = [ObjectId(oid_str) for oid_str in value]

    @property
    def user_write(self):
        return self._user_write

    @user_write.setter
    def user_write(self, value):
        self._user_write = [ObjectId(oid_str) for oid_str in value]

    @property
    def user_write_deny(self):
        return self._user_write_deny

    @user_write_deny.setter
    def user_write_deny(self, value):
        self._user_write_deny = [ObjectId(oid_str) for oid_str in value]

    @property
    def group_read(self):
        return self._group_read

    @group_read.setter
    def group_read(self, value):
        self._group_read = [ObjectId(oid_str) for oid_str in value]

    @property
    def group_write(self):
        return self._group_write

    @group_write.setter
    def group_write(self, value):
        self._group_write = [ObjectId(oid_str) for oid_str in value]
