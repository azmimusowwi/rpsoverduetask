import logging
import threading
import datetime
import os
import base64
from os import listdir
from configs.env_config import ENV_CONFIG
from queries.task_query import TaskQuery
import schedule
import time

app_config = ENV_CONFIG['app']
schedule_time = int(ENV_CONFIG['schedule']['estimation'])
task_repo = TaskQuery()

def do_process(task_id):
    try:
        task_repo.update_overdue_number(task_id)
    except Exception as exc:
        print('An exception occurred : ', str(exc))

def job():
    print('job is starting, please wait..')
    threads = []
    waiting_datas = []
    try:
        waiting_datas = task_repo.waiting_datas()
    except:
        print('An exception occurred')

    for waiting_data in waiting_datas:
        t = threading.Thread(target=do_process, args=(waiting_data['task_assignment_nid'],))
        threads.append(t)

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    print('job ended..')


if __name__ == "__main__":
    print('===== application run() =====')
    schedule.every(schedule_time).minutes.do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)
    print('===== application exited() =====')
