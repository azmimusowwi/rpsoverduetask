from pymongo import MongoClient
from configs.env_config import ENV_CONFIG

class DBMongo:
    def __init__(self):
        db_config = ENV_CONFIG['db_mongo']
        self.client = MongoClient(
            host=db_config['host'], port=db_config['port'], username=db_config['username'], password=db_config['password'])

    def close(self):
        self.client.close()

    def db(self):
        db_config = ENV_CONFIG['db_mongo']
        return self.client[db_config['database']]


main_mongo_conn = DBMongo()
main_mongo_db = main_mongo_conn.db()
main_db = main_mongo_db
