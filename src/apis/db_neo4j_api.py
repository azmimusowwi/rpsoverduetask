from neo4j import GraphDatabase
from neomodel import config

from configs.env_config import ENV_CONFIG


class DBNeo4j:
    def __init__(self):
        db_config = ENV_CONFIG['db_neo4j']
        host = db_config['host']
        port = db_config['port']
        user = db_config['user']
        password = db_config['password']

        uri = f'bolt://{host}:{port}'
        try:
            self._driver = GraphDatabase.driver(
                uri, auth=(db_config['user'], db_config['password']))
            url = f'bolt://{user}:{password}@{host}:{port}'
            config.DATABASE_URL = url
        except Exception as e:
            print(str(e))

    def close(self):
        self._driver.close()

    def write_transaction(self, transaction_function, payload):
        with self._driver.session() as session:
            result = session.write_transaction(transaction_function, payload)
            return result

    def write_transaction_no_param(self, transaction_function):
        with self._driver.session() as session:
            result = session.write_transaction(transaction_function)
            return result


main_neo4j_conn = DBNeo4j()
