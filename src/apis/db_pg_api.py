from sqlalchemy import create_engine, MetaData, Table
from configs.env_config import ENV_CONFIG
import psycopg2


class DBPostgres:

    @staticmethod
    def conn():
        dbConfig = ENV_CONFIG['db_pg']
        host = dbConfig['host']
        port = dbConfig['port']
        user = dbConfig['user']
        password = dbConfig['password']
        database = dbConfig['database']
        return psycopg2.connect(host=host, port=port, user=user, password=password, database=database)

    @staticmethod
    def engine():
        return create_engine('postgresql://', creator=DBPostgres.conn)

    @staticmethod
    def connect():
        return DBPostgres.engine().connect()

    @staticmethod
    def execute(statement, *multiparams, **params):
        conn = DBPostgres.connect()
        result = conn.execute(statement, *multiparams, **params)
        conn.close()
        return result

    @staticmethod
    def table(tableName):
        engine = DBPostgres.engine()
        metadata = MetaData(bind=engine)
        table = Table(tableName, metadata, autoload=True)
        return table
