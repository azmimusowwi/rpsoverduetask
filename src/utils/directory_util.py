import os
import errno
import shutil


class DirectoryUtil:

    def mkdir_p(self, path):
        """ 'mkdir -p' in Python """
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def move_dir(self, current, destination):
        shutil.move(current, destination)
        print('file is moved.')
