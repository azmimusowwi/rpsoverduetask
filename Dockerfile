FROM rpsoverduetask-dep:latest

WORKDIR /rpsoverduetask

COPY . .

#CMD ["gunicorn","-k","gevent","-w","8","--worker-connections","1000","-t","30","--chdir","./src/","--bind","0.0.0.0:6088","app:app"]

CMD ["python", "./src/app.py"]
